import React from 'react'
import './App.css'
import Product from 'features/Product'

function App () {
  return (
    <Product />
  )
}

export default App
