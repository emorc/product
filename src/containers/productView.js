import React from 'react'
import Container from './index'

export default function ProductView (props) {
  return (
    <Container className='container-product__detail'>{props.children}</Container>
  )
}
