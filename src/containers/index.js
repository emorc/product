import React from 'react'

export default function ProductPage (props) {
  return (
    <div className={props.className}>{props.children}</div>
  )
}
