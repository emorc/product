import React from 'react'
import Container from './index'

export default function name (props) {
  return (
    <Container className='container-product__main'>{props.children}</Container>
  )
}
