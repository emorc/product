import React from 'react'
import Header from 'components/header'
import Breadcrumbs from 'components/breadcrumbs'
import ProductPage from 'containers/productPage'
import ProductView from 'containers/productView'

import product from 'api/product.json'
import Carousel from 'components/carousel'

export default class Product extends React.Component {
  state = {
    categories: [],
    images: [],
    addedProducts: []
  }

  componentDidMount () {
    this.setState({
      ...product
    })
  }

  addToCart = () => {
    this.setState(prevState => {
      prevState.addedProducts.push(product)
      const products = prevState.addedProducts
      return {
        addedProducts: products
      }
    }, () => {
      alert('El producto se agregó a tu carrito')
    })
  }

  render () {
    return (
      <>
        <Header howMany={this.state.addedProducts.length} />
        <ProductPage>
          <Breadcrumbs categories={this.state.categories} />
          <ProductView>
            {
              this.state.images.length > 0 &&
              <Carousel onAdd={this.addToCart} {...this.state} />
            }
          </ProductView>
        </ProductPage>
      </>
    )
  }
}
