import React from 'react'
import Button from './button'

export default class Carousel extends React.Component {
  state = {
    selectedIndex: 0,
    selectedImage: ''
  }

  showImage = (index) => {
    return () => {
      const image = this.props.images[index].image
      this.setState({
        selectedIndex: index,
        selectedImage: image
      })
    }
  }

  componentDidMount () {
    this.showImage(0)()
  }

  render () {
    return (
      <>
        <ul className='thumbnails'>
          {
            this.props.images.map((image, index) => (
              <li key={image.thumbnail} className={this.state.selectedIndex === index ? 'selected' : ''}>
                <img src={image.thumbnail} onClick={this.showImage(index)} alt='thumbnail' />
              </li>
            ))}
        </ul>
        <div className='full-view'>
          <img src={this.state.selectedImage} alt='product' />
        </div>
        <div className='description' >
          <h1>{this.props.name}</h1>
          <strong>${this.props.price}</strong>
          <Button onClick={this.props.onAdd} className='button-add-to-cart'>Agregar al carrito</Button>
        </div>
      </>
    )
  }
}
