import React from 'react'

export default class Menu extends React.Component {
  render () {
    return <div className='menu'>
      <span>{this.props.text}</span>
      <ul>
        {
          this.props.options.map((item, index) => (
            <li key={`menu-${index}`}>
              {
                item.link
                  ? <a href={item.link} rel='norel noopener'>
                    {item.text}
                  </a>
                  : item.text
              }

            </li>
          ))
        }
      </ul>
    </div>
  }
}
