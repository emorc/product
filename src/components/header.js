import React from 'react'
import logo from 'assets/logo.png'
import Menu from './menu'
import CartIndicator from './cartIndicator'

const options = [{
  link: '/terms',
  text: 'Terms of services'
}]

export default class Header extends React.Component {
  state = {
    howMany: 0
  }

  componentDidUpdate (prevProps) {
    if (prevProps.howMany !== this.props.howMany) {
      console.log('entraaaa!!')

      this.setState({
        howMany: this.props.howMany
      })
    }
  }

  render () {
    return (
      <div className='App-header'>
        <img src={logo} alt='yema' />
        <Menu text='Categoría' options={options} />
        <Menu text='Categoría' options={options} />
        <CartIndicator addedProducts={this.state.howMany} />
      </div>
    )
  }
}
