import React from 'react'
import cart from 'assets/cart.png'

export default class CartIndicator extends React.Component {
  render () {
    return (
      <div className='cart'>
        <img src={cart} alt='cart' />
        <span>Carrito {this.props.addedProducts}</span>
      </div>
    )
  }
}
