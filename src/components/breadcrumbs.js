import React from 'react'

export default class Breadcrumbs extends React.Component {
  render () {
    return (
      <ul className='breadcrumbs'>
        <li>YEMA</li>
        {this.props.categories && this.props.categories.map(e => (
          <li key={e.link}><a href={e.link}>{e.name}</a></li>
        ))}
      </ul>
    )
  }
}
